using System;
using System.Collections;
using System.Linq;
using AventiaBackend.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace AventiaBackend.Controllers
{
    [Route("api/[controller]")]
    public class CategoryController : ControllerBase
    {
        private aventiaDbContext _context;

        public CategoryController(aventiaDbContext context)
        {
            _context = context;
        }

        // GET api/category
        [HttpGet]
        public ActionResult<IEnumerable> GetCategories()
        {
            return _context.Categories.Include(s => s.Clips).ToList();
        }

        // GET api/category/2
        [HttpGet("{id}")]
        public ActionResult<Category> Get(int id)
        {
            return _context.Categories.Include(s => s.Clips).SingleOrDefault(p => p.Id == id);
        }

        // POST api/category
        [HttpPost]
        public ActionResult CreateNewCategory(string name, int active)
        {
            var isActive = Convert.ToBoolean(active);

            _context.Add(new Category()
            {
                Name = name,
                Active = isActive
            });

            _context.SaveChanges();

            return Ok(new string[] { "status", "ok" });

        }

        // PUT api/category/2
        [HttpPut("{id}")]
        public ActionResult PutCategory(int id, string name, int active)
        {
            var existingCategory = _context.Categories
                    .Where(s => s.Id == id)
                    .FirstOrDefault<Category>();

            if (existingCategory != null)
            {
                var isActive = Convert.ToBoolean(active);
                
                existingCategory.Name = name;
                existingCategory.Active = isActive;

                _context.SaveChanges();

                return Ok(new string[] { "status", "updated" });
            }
            else
            {
                return NotFound($"ID: {id} not found");
            }
        }

        // DELETE api/category/2
        [HttpDelete("{id}")]
        public ActionResult DeleteCategory(int id)
        {
            if (id <= 0)
            {
                return BadRequest($"ID: {id} is not a valid catgory id");
            }

            var category = _context.Categories
                .Where(s => s.Id == id)
                .FirstOrDefault();

            if (category != null)
            {
                _context.Categories.Remove(category);
                _context.SaveChanges();

                return Ok();
            }
            else
            {
                return NotFound($"ID: {id} not found");
            }
        }
    }
}