using System;
using System.Collections;
using System.Linq;
using AventiaBackend.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;

namespace AventiaBackend.Controllers
{
    [Route("api/[controller]")]
    public class ClipsController : ControllerBase
    {
        private aventiaDbContext _context;

        public ClipsController(aventiaDbContext context)
        {
            _context = context;
        }

        // GET api/clips
        [HttpGet]
        public ActionResult<IEnumerable> GetClips()
        {
            return _context.Clips.ToList();
        }
        
        // GET api/clips/2
        [HttpGet("{id}")]
        public ActionResult<Clip> Get(int id)
        {
            return _context.Clips.SingleOrDefault(p => p.Id == id);
        }
        
        // POST api/clips
        [HttpPost]
        public ActionResult CreateNewClip(string name, int active, int cat_id)
        {
            var isActive = Convert.ToBoolean(active);

            _context.Add(new Clip()
            {
                Name = name,
                Active = isActive,
                CategoryId = cat_id
            });

            _context.SaveChanges();

            return Ok(new string[] { "status", "ok" });
        }
        
        // PUT api/clips/2
        [HttpPut("{id}")]
        public ActionResult PutClip(int id, string name, int active)
        {
            var existingClip = _context.Clips
                .Where(s => s.Id == id)
                .FirstOrDefault<Clip>();

            if (existingClip != null)
            {
                var isActive = Convert.ToBoolean(active);


                existingClip.Name = name;
                existingClip.Active = isActive;
//                existingClip.CategoryId = cat_id;

                _context.SaveChanges();

                return Ok(new string[] { "status", "updated" });
            }
            else
            {
                return NotFound($"ID: {id} not found");
            }
        }
        
        // DELETE api/clip/2
        [HttpDelete("{id}")]
        public ActionResult DeleteClip(int id)
        {
            if (id <= 0)
            {
                return BadRequest($"ID: {id} is not a valid clip id");
            }

            var clip = _context.Clips
                .Where(s => s.Id == id)
                .FirstOrDefault();

            if (clip != null)
            {
                _context.Clips.Remove(clip);
                _context.SaveChanges();

                return Ok();
            }
            else
            {
                return NotFound($"ID: {id} not found");
            }
        }
    }
}