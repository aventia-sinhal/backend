using Microsoft.EntityFrameworkCore;

namespace AventiaBackend.Models
{
    public class aventiaDbContext : DbContext
    {
        
        public aventiaDbContext() {}
        
        public aventiaDbContext(DbContextOptions<aventiaDbContext> options) : base(options) {}
        
        public DbSet<Category> Categories { get; set; }
        public DbSet<Clip> Clips { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>(entity =>
            {
                entity.ToTable("category", "aventia_app");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(11)");
                
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .IsUnicode(false);

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(1)");

            });

            modelBuilder.Entity<Clip>(entity =>
            {
                entity.ToTable("clips", "aventia_app");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(11)");
                
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .IsUnicode(false);

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(1)");
                
                entity.Property(e => e.CategoryId)
                    .HasColumnName("cat_id")
                    .HasColumnType("int(11)");
            });
        }
    }
}