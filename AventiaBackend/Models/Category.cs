using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AventiaBackend.Models
{
    
    public class Category
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }
        
        [Required]
        public bool Active { get; set; }
        
        public ICollection<Clip> Clips { get; set; }
    }
}