using System.ComponentModel.DataAnnotations;

namespace AventiaBackend.Models
{
    public class Clip
    {
        public int Id { get; set; }
        
        [Required]

        public string Name { get; set; }
        
        [Required]

        public bool Active { get; set; }
        
        public int CategoryId { get; set; }

    }
}